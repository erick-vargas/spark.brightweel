import React, { useReducer } from "react";
import ProductsReducer from "./ProductsReducer";
import ProductsContext from "./ProductsContext";
import axios from "axios";
import { api } from "./ProductsConstants";
import {
  GET_PRODUCTS,
  SEARCH_PRODUCTS,
  TOGGLE_STARRED,
  GET_PRODUCTS_STARRED,
} from "./ProductsTypes";

const ProductsState = ({ children }) => {
  const initialState = {
    products: [],
    productsStarred: [],
    pageNumber: 1,
    pageSize: 10,
    total: 1,
    searchValue: null,
    toggleStarred: null,
  };

  const [state, dispatch] = useReducer(ProductsReducer, initialState);

  const getProducts = async () => {
    const res = await axios.get(
      `${api}?type_like=product&_page=${state.pageNumber}&limit=${state.pageSize}`
    );

    dispatch({
      type: GET_PRODUCTS,
      payload: res,
    });
  };

  const searchProducts = async (value) => {
    const res = await axios.get(
      `${api}?type_like=product&name_like=${value}&_page=${state.pageNumber}&limit=${state.pageSize}`
    );

    dispatch({
      type: SEARCH_PRODUCTS,
      payload: { ...res, value },
    });
  };

  const getProductsStarred = async () => {
    const res = await axios.get(
      `${api}?type_like=product&starred_like=true&_page=${state.pageNumber}&limit=${state.pageSize}`
    );

    dispatch({
      type: GET_PRODUCTS_STARRED,
      payload: res,
    });
  };

  const toggleStarred = async (id, record) => {
    dispatch({
      type: TOGGLE_STARRED,
      payload: "loading",
    });

    await axios.put(`${api}/${id}`, { ...record, starred: !record.starred });

    dispatch({
      type: TOGGLE_STARRED,
      payload: null,
    });

    if (state.searchValue) {
      searchProducts(state.searchValue);
    } else {
      getProducts();
    }
  };

  return (
    <ProductsContext.Provider
      value={{
        products: state.products,
        productsStarred: state.productsStarred,
        pageNumber: state.pageNumber,
        pageSize: state.pageSize,
        total: state.total,
        searchValue: state.searchValue,
        getProducts,
        searchProducts,
        toggleStarred,
        getProductsStarred,
      }}
    >
      {children}
    </ProductsContext.Provider>
  );
};

export default ProductsState;
