import {
  GET_PRODUCTS,
  SEARCH_PRODUCTS,
  GET_PRODUCTS_STARRED,
} from "./ProductsTypes";

export default function reducer(state = {}, action = {}) {
  const { payload, type } = action;
  switch (type) {
    case GET_PRODUCTS:
    case SEARCH_PRODUCTS:
      return {
        ...state,
        products: payload.data,
        total: payload.headers["x-total-count"],
        searchValue: payload.value,
      };
    case GET_PRODUCTS_STARRED:
      return {
        ...state,
        productsStarred: payload.data,
      };
    default:
      return state;
  }
}
