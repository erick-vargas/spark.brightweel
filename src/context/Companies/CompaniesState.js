import React, { useReducer } from "react";
import CompaniesReducer from "./CompaniesReducer";
import CompaniesContext from "./CompaniesContext";
import axios from "axios";
import { api } from "./CompaniesConstants";
import {
  GET_COMPANIES,
  SEARCH_COMPANIES,
  TOGGLE_STARRED_COMPANIES,
  GET_COMPANIES_STARRED,
} from "./CompaniesTypes";

const CompaniesState = ({ children }) => {
  const initialState = {
    companies: [],
    companiesStarred: [],
    pageNumber: 1,
    pageSize: 10,
    total: 1,
    searchValue: null,
    toggleStarred: null,
  };

  const [state, dispatch] = useReducer(CompaniesReducer, initialState);

  const getCompanies = async () => {
    const res = await axios.get(
      `${api}?type_like=company&_page=${state.pageNumber}&limit=${state.pageSize}`
    );

    dispatch({
      type: GET_COMPANIES,
      payload: res,
    });
  };

  const searchCompanies = async (value) => {
    const res = await axios.get(
      `${api}?type_like=company&name_like=${value}&_page=${state.pageNumber}&limit=${state.pageSize}`
    );

    dispatch({
      type: SEARCH_COMPANIES,
      payload: { ...res, value },
    });
  };

  const getCompaniesStarred = async () => {
    const res = await axios.get(
      `${api}?type_like=company&starred_like=true&_page=${state.pageNumber}&limit=${state.pageSize}`
    );

    dispatch({
      type: GET_COMPANIES_STARRED,
      payload: res,
    });
  };

  const toggleStarred = async (id, record) => {
    dispatch({
      type: TOGGLE_STARRED_COMPANIES,
      payload: "loading",
    });

    await axios.put(`${api}/${id}`, { ...record, starred: !record.starred });

    dispatch({
      type: TOGGLE_STARRED_COMPANIES,
      payload: null,
    });

    if (state.searchValue) {
      searchCompanies(state.searchValue);
    } else {
      getCompanies();
    }
  };

  return (
    <CompaniesContext.Provider
      value={{
        companies: state.companies,
        companiesStarred: state.companiesStarred,
        pageNumber: state.pageNumber,
        pageSize: state.pageSize,
        total: state.total,
        searchValue: state.searchValue,
        getCompanies,
        searchCompanies,
        toggleStarred,
        getCompaniesStarred,
      }}
    >
      {children}
    </CompaniesContext.Provider>
  );
};

export default CompaniesState;
