import {
  GET_COMPANIES,
  SEARCH_COMPANIES,
  GET_COMPANIES_STARRED,
} from "./CompaniesTypes";

export default function reducer(state = {}, action = {}) {
  const { payload, type } = action;
  switch (type) {
    case GET_COMPANIES:
    case SEARCH_COMPANIES:
      return {
        ...state,
        companies: payload.data,
        total: payload.headers["x-total-count"],
        searchValue: payload.value,
      };
    case GET_COMPANIES_STARRED:
      return {
        ...state,
        companiesStarred: payload.data,
      };
    default:
      return state;
  }
}
