import React, { useReducer } from "react";
import AnimalsReducer from "./AnimalsReducer";
import AnimalsContext from "./AnimalsContext";
import axios from "axios";
import { api } from "./AnimalConstants";
import {
  GET_ANIMALS,
  SEARCH_ANIMALS,
  TOGGLE_STARRED_ANIMALS,
  GET_ANIMALS_STARRED,
} from "./AnimalTypes";

const AnimalsState = (props) => {
  const initialState = {
    animals: [],
    animalsStarred: [],
    pageNumber: 1,
    pageSize: 10,
    total: 1,
    searchValue: null,
    toggleStarred: null,
  };

  const [state, dispatch] = useReducer(AnimalsReducer, initialState);

  const getAnimals = async () => {
    const res = await axios.get(
      `${api}?type_like=animal&_page=${state.pageNumber}&limit=${state.pageSize}`
    );

    dispatch({
      type: GET_ANIMALS,
      payload: res,
    });
  };

  const searchAnimals = async (value) => {
    const res = await axios.get(
      `${api}?type_like=animal&name_like=${value}&_page=${state.pageNumber}&limit=${state.pageSize}`
    );

    dispatch({
      type: SEARCH_ANIMALS,
      payload: { ...res, value },
    });
  };

  const getAnimalsStarred = async () => {
    const res = await axios.get(
      `${api}?type_like=animal&starred_like=true&_page=${state.pageNumber}&limit=${state.pageSize}`
    );

    dispatch({
      type: GET_ANIMALS_STARRED,
      payload: res,
    });
  };

  const toggleStarred = async (id, record) => {
    dispatch({
      type: TOGGLE_STARRED_ANIMALS,
      payload: "loading",
    });

    await axios.put(`${api}/${id}`, { ...record, starred: !record.starred });

    dispatch({
      type: TOGGLE_STARRED_ANIMALS,
      payload: null,
    });

    if (state.searchValue) {
      searchAnimals(state.searchValue);
    } else {
      getAnimals();
    }
  };

  return (
    <AnimalsContext.Provider
      value={{
        animals: state.animals,
        animalsStarred: state.animalsStarred,
        pageNumber: state.pageNumber,
        pageSize: state.pageSize,
        total: state.total,
        searchValue: state.searchValue,
        getAnimals,
        searchAnimals,
        toggleStarred,
        getAnimalsStarred,
      }}
    >
      {props.children}
    </AnimalsContext.Provider>
  );
};

export default AnimalsState;
