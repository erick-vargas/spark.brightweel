import {
  GET_ANIMALS,
  SEARCH_ANIMALS,
  GET_ANIMALS_STARRED,
} from "./AnimalTypes";

export default function reducer(state = {}, action = {}) {
  const { payload, type } = action;
  switch (type) {
    case GET_ANIMALS:
    case SEARCH_ANIMALS:
      return {
        ...state,
        animals: payload.data,
        total: payload.headers["x-total-count"],
        searchValue: payload.value,
      };
    case GET_ANIMALS_STARRED:
      return {
        ...state,
        animalsStarred: payload.data,
      };
    default:
      return state;
  }
}
