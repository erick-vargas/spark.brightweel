import React, { useReducer } from "react";
import MainReducer from "./MainReducer";
import MainContext from "./MainContext";
import axios from "axios";
import { api } from "./MainConstants";
import {
  GET_RECORDS,
  SEARCH_RECORDS,
  TOGGLE_STARRED_RECORDS,
  GET_RECORDS_STARRED,
} from "./MainTypes";

const MainState = (props) => {
  const initialState = {
    records: [],
    recordsStarred: [],
    pageNumber: 1,
    pageSize: 10,
    total: 1,
    searchValue: null,
    toggleStarred: null,
  };

  const [state, dispatch] = useReducer(MainReducer, initialState);

  const getRecords = async () => {
    const res = await axios.get(
      `${api}?&_page=${state.pageNumber}&limit=${state.pageSize}`
    );

    dispatch({
      type: GET_RECORDS,
      payload: res,
    });
  };

  const searchRecords = async (value) => {
    const res = await axios.get(
      `${api}?&name_like=${value}&_page=${state.pageNumber}&limit=${state.pageSize}`
    );

    dispatch({
      type: SEARCH_RECORDS,
      payload: { ...res, value },
    });
  };

  const getRecordsStarred = async () => {
    const res = await axios.get(
      `${api}?&starred_like=true&_page=${state.pageNumber}&limit=${state.pageSize}`
    );

    dispatch({
      type: GET_RECORDS_STARRED,
      payload: res,
    });
  };

  const toggleStarred = async (id, record) => {
    dispatch({
      type: TOGGLE_STARRED_RECORDS,
      payload: "loading",
    });

    await axios.put(`${api}/${id}`, { ...record, starred: !record.starred });

    dispatch({
      type: TOGGLE_STARRED_RECORDS,
      payload: null,
    });

    if (state.searchValue) {
      searchRecords(state.searchValue);
    } else {
      getRecords();
    }
  };

  return (
    <MainContext.Provider
      value={{
        records: state.records,
        recordsStarred: state.recordsStarred,
        pageNumber: state.pageNumber,
        pageSize: state.pageSize,
        total: state.total,
        searchValue: state.searchValue,
        getRecords,
        searchRecords,
        toggleStarred,
        getRecordsStarred,
      }}
    >
      {props.children}
    </MainContext.Provider>
  );
};

export default MainState;
