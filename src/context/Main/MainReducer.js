import {
  GET_RECORDS,
  SEARCH_RECORDS,
  GET_RECORDS_STARRED,
} from "./MainTypes";

export default function reducer(state = {}, action = {}) {
  const { payload, type } = action;

  switch (type) {
    case GET_RECORDS:
      case SEARCH_RECORDS:
      return {
        ...state,
        records: payload.data,
        total: payload.headers["x-total-count"],
        searchValue: payload.value,
      };
    case GET_RECORDS_STARRED:
      return {
        ...state,
        recordsStarred: payload.data,
      };
    default:
      return state;
  }
}
