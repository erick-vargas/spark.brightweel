import { Routes, Route } from "react-router-dom";
import Products from "./components/Products";
import Animals from "./components/Animals";
import Home from "./components/Home";
import Companies from "./components/Companies";
import ProductsState from "./context/Products/ProductsState";
import AnimalsState from "./context/Animals/AnimalsState";
import CompaniesState from "./context/Companies/CompaniesState";
import MainState from "./context/Main/MainState";

const App = () => {
  return (

    <Routes>
      <Route path="/" element={<MainState><Home /></MainState>} />
      <Route path="/products" element={<ProductsState><Products /></ProductsState>} />
      <Route path="/animals" element={<AnimalsState><Animals /></AnimalsState>} />
      <Route path="/companies" element={<CompaniesState><Companies /></CompaniesState>} />
    </Routes>
  );
};

export default App;
