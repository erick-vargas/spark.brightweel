import React, { useEffect, useContext } from "react";
import { Table } from "antd";
import AnimalsContext from "../../context/Animals/AnimalsContext";
import Layout from "../Layout";
import { SearchWrapper } from "../Products/Products";
import Search from "../Search";
import settings from "./TableSettings";

const Animals = () => {
  const {
    animals,
    getAnimals,
    getAnimalsStarred,
    searchAnimals,
    pageNumber,
    pageSize,
    total,
    searchValue,
    toggleStarred,
    animalsStarred,
  } = useContext(AnimalsContext);

  useEffect(() => {
    getAnimals();
  }, []);

  useEffect(() => {
    getAnimalsStarred();
  }, [animals]);

  return (
    <Layout>
      <SearchWrapper>
        <Search
          action={searchAnimals}
          placeholder="Write some..."
          label="Search Animals"
          value={searchValue}
          options={animals ? animals : null}
        />
        <h3>Starred: {animalsStarred ? animalsStarred.length : 0}</h3>
      </SearchWrapper>
      <Table
        onRow={(record) => {
          return {
            onClick: () => {
              toggleStarred(record.id, record);
            },
          };
        }}
        columns={settings()}
        dataSource={animals}
        rowKey={"id"}
        pagination={{
          current: pageNumber,
          pageSize: pageSize,
          total: total ? total : 1,
        }}
      />
    </Layout>
  );
};

export default Animals;
