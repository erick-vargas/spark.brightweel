import React from "react";
import { StarOutlined, StarFilled } from "@ant-design/icons";

const TableSettings = () => [
  {
    title: "Name",
    dataIndex: "name",
  },
  {
    title: "Scientific Name",
    dataIndex: "taxonomy",
    render: (taxonomy) => taxonomy.scientificName,
  },
  {
    title: "Image",
    dataIndex: "image",
    render: (avatar) => (avatar ? <img src={avatar} width="30" alt="" /> : ""),
  },
  {
    title: "Starred",
    dataIndex: "starred",
    render: (starred) => (starred ? <StarFilled /> : <StarOutlined />),
  },
];

export default TableSettings;
