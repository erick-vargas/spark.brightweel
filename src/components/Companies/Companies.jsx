import React, { useEffect, useContext } from "react";
import { Table } from "antd";
import CompaniesContext from "../../context/Companies/CompaniesContext";
import Layout from "../Layout";
import { SearchWrapper } from "../Products/Products";
import Search from "../Search";
import settings from "./TableSettings";

const Companies = () => {
  const {
    companies,
    getCompanies,
    getCompaniesStarred,
    searchCompanies,
    pageNumber,
    pageSize,
    total,
    searchValue,
    toggleStarred,
    companiesStarred,
  } = useContext(CompaniesContext);

  useEffect(() => {
    getCompanies();
  }, []);

  useEffect(() => {
    getCompaniesStarred();
  }, [companies]);

  return (
    <Layout>
      <SearchWrapper>
        <Search
          action={searchCompanies}
          placeholder="Write some..."
          label="Search Companies"
          value={searchValue}
          options={companies ? companies : null}
        />
        <h3>Starred: {companiesStarred ? companiesStarred.length : 0}</h3>
      </SearchWrapper>
      <Table
        onRow={(record) => {
          return {
            onClick: () => {
              toggleStarred(record.id, record);
            },
          };
        }}
        columns={settings()}
        dataSource={companies}
        rowKey={"id"}
        pagination={{
          current: pageNumber,
          pageSize: pageSize,
          total: total ? total : 1,
        }}
      />
    </Layout>
  );
};

export default Companies;
