import React from 'react'
import {
    StarOutlined,
    StarFilled,
  } from '@ant-design/icons';

const tableSettings = () => [
    {
        title: 'Name',
        dataIndex: 'name',
    },
    {
        title: 'Description',
        dataIndex: 'description',
    },
    {
        title: 'Address',
        dataIndex: 'address',
        render: (address) => <>
            <p>{`
                ${address.address1 ? address.address1 : '-'}
                ${address.address2 ? address.address2 : '-'}
                ${address.city ? 'City: ' + address.city : '-'}
                ${address.state ? 'State: ' + address.state : '-'}
            `}</p>
        </>
    },
    {
        title: 'Starred',
        dataIndex: 'starred',
        render: (starred) => starred ? <StarFilled /> : <StarOutlined />
    },
];

export default tableSettings