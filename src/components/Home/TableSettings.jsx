import React from "react";
import { StarOutlined, StarFilled } from "@ant-design/icons";

const TableSettings = () => [
  {
    title: "Name",
    dataIndex: "name",
  },
  {
    title: "Type",
    dataIndex: "type",
  },
  {
    title: "Category",
    dataIndex: "productCategory",
  },
  {
    title: "Preview Text",
    dataIndex: "previewText",
  },
  {
    title: "Scientific Name",
    dataIndex: "taxonomy",
    render: (taxonomy) => (taxonomy ? taxonomy.scientificName : null),
  },
  {
    title: "Description",
    dataIndex: "description",
  },
  {
    title: "Address",
    dataIndex: "address",
    render: (address) =>
      address && (
        <>
          <p>{`
              ${address.address1 ? address.address1 : "-"}
              ${address.address2 ? address.address2 : "-"}
              
          `}</p>
          <p>
            ${address.city ? "City: " + address.city : "-"}$
            {address.state ? "State: " + address.state : "-"}
          </p>
        </>
      ),
  },
  {
    title: "Image",
    dataIndex: "image",
    render: (avatar) => (avatar ? <img src={avatar} width="30" alt="" /> : ""),
  },
  {
    title: "Starred",
    dataIndex: "starred",
    render: (starred) => (starred ? <StarFilled /> : <StarOutlined />),
  },
];

export default TableSettings;
