import React, { useEffect, useContext } from "react";
import { Table } from "antd";
import MainContext from "../../context/Main/MainContext";
import Layout from "../Layout";
import { SearchWrapper } from "../Products/Products";
import Search from "../Search";
import settings from "./TableSettings";

const Home = () => {
  const {
    records,
    getRecords,
    getRecordsStarred,
    searchRecords,
    pageNumber,
    pageSize,
    total,
    searchValue,
    toggleStarred,
    recordsStarred,
  } = useContext(MainContext);

  useEffect(() => {
    getRecords();
  }, []);

  useEffect(() => {
    getRecordsStarred();
  }, [records]);

  return (
    <Layout>
      <SearchWrapper>
        <Search
          action={searchRecords}
          placeholder="Write some..."
          label="Search Items"
          value={searchValue}
          options={records ? records : null}
        />
        <h3>Starred: {recordsStarred ? recordsStarred.length : 0}</h3>
      </SearchWrapper>
      <Table
        onRow={(record) => {
          return {
            onClick: () => {
              toggleStarred(record.id, record);
            },
          };
        }}
        columns={settings()}
        dataSource={records}
        rowKey={"id"}
        pagination={{
          current: pageNumber,
          pageSize: pageSize,
          total: total ? total : 1,
        }}
      />
    </Layout>
  );
};

export default Home;
