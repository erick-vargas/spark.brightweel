import React, { useState } from 'react';
import { Layout, Menu } from 'antd';
import {
  HomeOutlined,
  ThunderboltOutlined,
  GiftOutlined,
  ShopOutlined
} from '@ant-design/icons';

import { Link, useLocation } from 'react-router-dom';
import Yoda from '../Yoda';

const { Sider } = Layout;

const App = () => {
  const [collapsed, setCollapsed] = useState(false);
  const location = useLocation();

  const onCollapse = (collapsed) => setCollapsed(collapsed);
  return (
    <Sider collapsible collapsed={collapsed} onCollapse={onCollapse}>
      <div className="logo" />
      <Menu
        theme="dark"
        mode="inline"
        defaultSelectedKeys={[location.pathname]}
      >
        <Menu.Item key="/">
          <HomeOutlined />
          <span>Home</span>
          <Link to="/"></Link>
        </Menu.Item>

        <Menu.Item key="/products">
          <GiftOutlined />
          <span>Products</span>
          <Link to="/products"></Link>
        </Menu.Item>

        <Menu.Item key="/animals">
          <ShopOutlined />
          <span>Animals</span>
          <Link to="/animals"></Link>
        </Menu.Item>

        <Menu.Item key="/companies">
          <ThunderboltOutlined />
          <span>Companies</span>
          <Link to="/companies"></Link>
        </Menu.Item>
      </Menu>
      {!collapsed && <Yoda />}
    </Sider>
  );
};

export default App;
