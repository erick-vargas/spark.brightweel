import React from 'react'
import styled from 'styled-components'

const CustomHeader = styled.header`
    min-height: 80px;
    background: #ccc;
    padding: 10px;
    display: flex;
    align-items: center;

    .title{
        color: #1890ff;
        font-size: 30px;
        text-transform: uppercase;
    }
`

const Header = () => (
    <CustomHeader><h1 className="title">Brightwheel</h1></CustomHeader>
)

export default Header;