import React from 'react';

import { Layout } from 'antd';

const Footer = () => {
  return (
    <Layout.Footer style={{ textAlign: 'center' }}>erickorso 2022</Layout.Footer>
  );
};

export default Footer;
