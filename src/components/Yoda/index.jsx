import React from "react";
import styled from "styled-components";
import image from "../../yoda.png";

const ImageWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-content: center;

  img {
    width: 100%;
    height: 300px;
  }
`;

const Yoda = () => (
  <ImageWrapper>
    <img src={image} />
  </ImageWrapper>
);

export default Yoda;
