import React from 'react'
import {
    StarOutlined,
    StarFilled,
  } from '@ant-design/icons';

const tableSettings = () => [
    {
        title: 'Name',
        dataIndex: 'name',
    },
    {
        title: 'Category',
        dataIndex: 'productCategory',
    },
    {
        title: 'Preview Text',
        dataIndex: 'previewText',
    },
    {
        title: 'Image',
        dataIndex: 'image',
        render: (avatar) => avatar ? <img src={avatar} width="30" alt=""/> : ''
    },
    {
        title: 'Starred',
        dataIndex: 'starred',
        render: (starred) => starred ? <StarFilled /> : <StarOutlined />
    },
];

export default tableSettings