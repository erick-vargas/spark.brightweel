import React, { useEffect, useContext } from "react";
import styled from "styled-components";
import { Table } from "antd";
import ProductsContext from "../../context/Products/ProductsContext";
import Layout from "../Layout";
import Search from "../Search";
import settings from "./TableSettings";

export const SearchWrapper = styled.div`
  max-width: 400px;
  margin-bottom: 20px;

  h3 {
    margin-top: 10px;
  }
`;

const Products = () => {
  const {
    products,
    getProducts,
    getProductsStarred,
    searchProducts,
    pageNumber,
    pageSize,
    total,
    searchValue,
    toggleStarred,
    productsStarred,
  } = useContext(ProductsContext);

  useEffect(() => {
    getProducts();
  }, []);
  
  useEffect(() => {
    getProductsStarred()
  }, [products]);

  return (
    <Layout>
      <SearchWrapper>
        <Search
          action={searchProducts}
          placeholder="Write some..."
          label="Search Products"
          value={searchValue}
          options={products ? products : null}
        />
        <h3>Starred: {productsStarred ? productsStarred.length : 0}</h3>
      </SearchWrapper>
      <Table
        onRow={(record) => {
          return {
            onClick: () => {
              toggleStarred(record.id, record);
            },
          };
        }}
        columns={settings()}
        dataSource={products}
        rowKey={"id"}
        pagination={{
          current: pageNumber,
          pageSize: pageSize,
          total: total ? total : 1,
        }}
      />
    </Layout>
  );
};

export default Products;
