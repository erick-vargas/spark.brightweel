import React from "react";
import { Input } from "antd";
import { AutoComplete } from "antd";

const SearchComponent = ({ action, placeholder, label, options }) => {
  const onSearch = (value) => action(value);

  const handleKeyPress = (e) => {
    action(e.target.value);
  };

  const dataList = options
    ? options.map((option) => ({ value: option.name, label: option.name }))
    : [];

  return (
    <AutoComplete options={dataList} onSelect={onSearch}>
      <Input
        placeholder={placeholder ? placeholder : "input search text"}
        allowClear
        size="large"
        onKeyPress={handleKeyPress}
        datalist={dataList}
      />
    </AutoComplete>
  );
};

export default SearchComponent;
